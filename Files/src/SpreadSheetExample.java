import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SpreadSheetExample {
	
	public static void main(String args[]) throws IOException{
		
		FileWriter w = new FileWriter("spreadsheet.csv");
		
		w.write("S.no,Name,Address\n");
		w.write("1,Saisree,Hyderabad\n");
		w.write("2,Sai,Hyderabad");
		w.close();
		
		FileReader r = new FileReader("spreadsheet.csv");
		
		int c;
		
		while((c= r.read())!=-1){
			System.out.print((char)c);
		}
	}

}
