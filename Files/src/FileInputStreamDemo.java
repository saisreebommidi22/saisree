import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamDemo {
	
	public static void main(String[] args) throws IOException {
		
		FileInputStream input = new FileInputStream("/home/user/Desktop/eclipse/Files/sai.txt") ;
		
		int c;
		
		while((c= input.read())!=-1){
			System.out.print((char)c);
		}
	}

}
