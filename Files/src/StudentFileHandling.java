import java.io.FileWriter;

import java.io.IOException;

public class StudentFileHandling {
	
	public static void main(String[] args) throws IOException {
		
		Student s1= new Student("Sai", 98, 98, 62);
		Student s2= new Student("Saisree", 98, 98, 62);
		Student s3= new Student("Sree", 98, 98, 62);
		
		FileWriter w = new FileWriter("Student.csv");
		w.write("Id,name,Sub1,sub2,sub3,Percentage\n");
		w.write(s1.toString());
		w.write(s2.toString());
		w.write(s3.toString());
		w.close();

	}

}
