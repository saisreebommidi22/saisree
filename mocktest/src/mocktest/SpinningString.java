package mocktest;

public class SpinningString{

	public static void  main(String[] arg) {
		
		
		System.out.println(SpinningString.rotate("talent", 1));
		System.out.println(SpinningString.rotate("talent", 2));
		System.out.println(SpinningString.rotate("talent", 3));
		
		
	}

	public static String rotate(String str, int no_of_positions) {
	
		if(str == null)
		{
			
			String str1=null;
			
			return str1;
		}
		else if((no_of_positions <= 0) &&  (str.length() < no_of_positions))
		{
			
			return str;
		}
		else
		{
			int string_length = str.length();
			int difference = string_length-no_of_positions;
			String substring1 = str.substring(0, difference);
			String substring2 = str.substring(difference,string_length);
			str =substring2.concat(substring1);
			return str;
		}
			
	}
}

