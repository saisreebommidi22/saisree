package package1;

public class Student {

	private int SId;
	private String name;
	private long phNo;
	private String s_class;
	private float m1,m2,m3;
		// TODO Auto-generated constructor stub
	
	public void setSId(int SId) {

		this.SId = SId;
	}

	public void setName(String name) {

		this.name = name;
	}

	public void setphNo(long phNo) {

		this.phNo = phNo;
	}

	public void setS_class(String s_class) {

		this.s_class = s_class;
	}

	public void setM1(float m1) {

		this.m1 = m1;
	}

	public void setM2(int m2) {

		this.m2 = m2;
	}

	public void setM3(int m1) {

		this.m3 = m3;
	}

	public int getSId() {

		return SId;
	}

	public String getName() {

		return name;
	}

	public long getphNo() {

		return phNo;
	}

	public String getS_class() {

		return s_class;

	}
	public float getM1() {

		return m1;
	}
	public float getM2() {

		return m2;
	}
	public float getM3() {

		return m3;
	}
	public double getAverage(){
		
		return (m1+m2+m3)/3;
	}
	public String toString(){
		
		return "Student Details : \nID : "+SId+ "\nName : "+name+"\nClass : "+s_class+"\n Phno : "+phNo+ "\nM1 : "+m1+ "\nM2 : "+m2+ "\nM3 : "+m3+ "\nAverage : "+getAverage();

	}



}
