package interface_Inheritance;

public class Class11 extends Student {

	private int m_theory;

	private int m_practical;

	private int p_theory;

	private int p_practical;

	private int c_theory;

	private int c_practical;

	private int english;

	private int hindi;
	

	public Class11(String name,int m_theory, int m_practical, int p_theory, int p_practical, int c_theory, int c_practical,
			int english, int hindi) {
		
		super(name);
		this.m_theory = m_theory;
		this.m_practical = m_practical;
		this.p_theory = p_theory;
		this.p_practical = p_practical;
		this.c_theory = c_theory;
		this.c_practical = c_practical;
		this.english = english;
		this.hindi = hindi;
		
	}

	public int getM_theory() {
		
		return m_theory;
	}

	public void setM_theory(int m_theory) {
		
		this.m_theory = m_theory;
	}

	public int getM_practical() {
		
		return m_practical;
	}

	public void setM_practical(int m_practical) {
		
		this.m_practical = m_practical;
	}

	public int getP_theory() {
		
		return p_theory;
	}

	public void setP_theory(int p_theory) {
		
		this.p_theory = p_theory;
	}

	public int getP_practical() {
		
		return p_practical;
	}

	public void setP_practical(int p_practical) {
		
		this.p_practical = p_practical;
	}

	public int getC_theory() {
		
		return c_theory;
	}

	public void setC_theory(int c_theory) {
		
		this.c_theory = c_theory;
	}

	public int getC_practical() {
		
		return c_practical;
	}

	public void setC_practical(int c_practical) {
		
		this.c_practical = c_practical;
	}

	public int getEnglish() {
		
		return english;
	}

	public void setEnglish(int english) {
		
		this.english = english;
	}

	public int getHindi() {
		
		return hindi;
	}

	public void setHindi(int hindi) {
		
		this.hindi = hindi;
	}

	@Override
	public float findPercentage() {
		// TODO Auto-generated method stub
		float total = p_practical+m_practical+c_practical+m_theory+p_theory+c_theory;
		
		float percentage = (total/800)*100;
		
		return percentage;
	}

	@Override
	public String toString() {
		
		System.out.println(super.toString());
		return "Class11 [m_theory=" + m_theory + ", m_practical=" + m_practical + ", p_theory=" + p_theory
				+ ", p_practical=" + p_practical + ", c_theory=" + c_theory + ", c_practical=" + c_practical
				+ ", english=" + english + ", hindi=" + hindi + "Percentage"+findPercentage()+"]";
	}
	

}
[]