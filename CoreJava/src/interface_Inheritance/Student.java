package interface_Inheritance;

abstract class Student {
	
	private int id;
	
	private String name;
	
	static int idGenerator = 101;
	
	Student(){
		
	}
	
	public Student(String name) {
		
		this.id = idGenerator++;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public abstract float findPercentage();

	@Override
	public String toString() {
		
		return "Student [id=" + id + ", name=" + name + "]";
	}
	
	
	

}
