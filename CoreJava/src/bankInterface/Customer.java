package bankInterface;

public class Customer {
	
	private double investment,tenure;
	
	Customer(){
		
	}

	public Customer(double investment, double tenure) {
		super();
		
		this.investment = investment;
		this.tenure = tenure;
	}

	public double getInvestment() {
		return investment;
	}

	public void setInvestment(double investment) {
		this.investment = investment;
	}

	public double getTenure() {
		return tenure;
	}

	public void setTenure(double tenure) {
		this.tenure = tenure;
	}
	public String toString(){
		 
		return "Customer [ investment ="+getInvestment()+"tenure ="+getTenure()+"]";
		
	}
	

}
