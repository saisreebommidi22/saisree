package bankInterface;

public class SBI implements Bank{
	
	private double rateOfInterest;
	private Customer customer;
	
	public SBI(Customer customer){
		
		this.customer= customer;
	}

	public double getRateOfInterest() {
		
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		
		this.rateOfInterest = rateOfInterest;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Override
	public double calcROI() {
		// TODO Auto-generated method stub
		rateOfInterest = (customer.getTenure()/customer.getInvestment())*100;
		return rateOfInterest;
	}
	public String toString(){
		
		return "SBI [ customer = "+customer+ ",Rate oF interest = "+calcROI()+"]";
	}


}
