package map;

import java.util.Comparator;
import java.util.Map.Entry;

public class BookComparator  implements Comparator<Entry<Integer, Book>>{

	public int compare(Entry<Integer, Book> b1, Entry<Integer, Book> b2) {
		// TODO Auto-generated method stub
	
		return (b1.getValue()).getAuthor().compareTo((b2.getValue()).getAuthor());
	}


}
