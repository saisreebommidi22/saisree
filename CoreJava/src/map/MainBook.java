package map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainBook {
	
	public static void main(String[] args) {

	Map<Integer,Book> map = new HashMap<Integer,Book>();

	Book b1 = new Book("C","Bala Guru Swami");
	Book b2 = new Book("Java","James Gosling");	
	Book b3 = new Book("HarryPotter","JK Royling");	

	map.put(b1.getId(), b1);
	map.put(b2.getId(), b2);
	map.put(b3.getId(), b3);

	for(Map.Entry<Integer, Book> entry : map.entrySet()){

	System.out.println(entry.getKey()+ " : " +entry.getValue());
	}
	getSortedMapByList(map);
	


	}
	public static void getSortedMapByList(Map<Integer, Book> map) {
		
		List<Map.Entry<Integer, Book>> mapList = new ArrayList<Map.Entry<Integer, Book>>();
		
		for (Map.Entry<Integer, Book> entry : map.entrySet()) {

			mapList.add(entry);
		}
		for (Map.Entry<Integer, Book> entry : mapList) {

			System.out.println(entry);
		}
		Collections.sort(mapList, new BookComparator());
		System.out.println("================================");

		for (Map.Entry<Integer, Book> entry : mapList) {

			System.out.println(entry);
		}



	}



}
