package map;

import java.util.*;

import javax.swing.ListCellRenderer;

public class MapDemo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("enter no.of elements");

		int n = sc.nextInt();

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		System.out.println("Enter elements");

		for (int i = 0; i < n; i++) {

			map.put(i, sc.nextInt());
		}
		System.out.println(map);
		
		getSortedMapByList(map);
	}

	public static void getSortedMapByList(Map<Integer, Integer> map) {

		List<Map.Entry<Integer, Integer>> mapList = new ArrayList<Map.Entry<Integer, Integer>>();

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {

			mapList.add(entry);
		}
		for (Map.Entry<Integer, Integer> entry : mapList) {

			System.out.println(entry);
		}
		Collections.sort(mapList, new ListComparator());

		System.out.println("================================");

		for (Map.Entry<Integer, Integer> entry : mapList) {

			System.out.println(entry);
		}

	}

}
