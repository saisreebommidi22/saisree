package map;

import java.util.*;
import java.util.Map.Entry;

public class ListComparator implements Comparator<Map.Entry<Integer, Integer>>{

	@Override
	public int compare(Entry<Integer, Integer> entry1,Entry<Integer, Integer> entry2 ) {
		// TODO Auto-generated method stub
		
		if(entry1.getValue() < entry2.getValue())
			
			return -1;
		
		else if(entry1.getValue() > entry2.getValue())
			
			return 1;
		
		return 0;
	}


}
