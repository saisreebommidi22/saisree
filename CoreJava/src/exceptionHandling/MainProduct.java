package exceptionHandling;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class MainProduct {

	public static void main(String[] args) throws InvalidProductException {

		Scanner sc = new Scanner(System.in);

		Product product1 = new Product(100, 100);
		Product product2 = new Product(800, 200);
		Product product3 = new Product(80, 300);
		Product product4 = new Product(220, 300);
		Product product5 = new Product(200, 494);
		Product product6 = new Product(300, 383);
		Product product7 = new Product(500, 383);
		Product product8 = new Product(600, 8212);
		Product product9 = new Product(400, 3984);
		Product product10 = new Product(180, 9038);

		List<Product> proList = new ArrayList<Product>();

		proList.add(product1);
		proList.add(product2);
		proList.add(product3);
		proList.add(product4);
		proList.add(product5);
		proList.add(product6);
		proList.add(product7);
		proList.add(product8);
		proList.add(product9);
		proList.add(product10);

		System.out.println(proList + " ");
		System.out.println("==================================");
		System.out.println();
		System.out.println("Products ");

		Iterator<Product> it = proList.iterator();
		while (it.hasNext()) {

			Product p = it.next();
			try {

				if (p.getWeight() < 200) {

					throw new InvalidProductException("Invalid Product Exception");
				}
			}

			catch (InvalidProductException e) {
				it.remove();
			}
		}
		for (Product i : proList) {
			System.out.println(i);

		}

	}
}
