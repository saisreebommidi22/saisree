package exceptionHandling;

import java.util.Scanner;

public class VoterEligibility {
	
	public static void main(String[] args) throws InvalidAgeException {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter age :");
		
		int age = sc.nextInt();
		
		if(age<18)
			
			throw new InvalidAgeException("Age must be greater than 18");
		else
			System.out.println("You are eligible for voting");
	}

}
