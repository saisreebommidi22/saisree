package cricketInterface;

public interface Cricket {
	
	public double averageScore();
	
	public double averageFallOfWickets();
	
	public double calcStrikeRate();

}
