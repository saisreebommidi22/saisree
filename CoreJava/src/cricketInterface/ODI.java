package cricketInterface;

public class ODI implements Cricket {
	
	private double innings;
	
	private double overs_per_innings;
	
	private double runs_per_innings;
	
	private double fall_of_wickets;
	
	ODI(){
		
	}

	public ODI(double innings, double overs_per_innings, double runs_per_innings, double fall_of_wickets) {
		super();
		this.innings = innings;
		this.overs_per_innings = overs_per_innings;
		this.runs_per_innings = runs_per_innings;
		this.fall_of_wickets = fall_of_wickets;
	}

	public double getInnings() {
		return innings;
	}

	public void setInnings(double innings) {
		this.innings = innings;
	}

	public double getOvers_per_innings() {
		return overs_per_innings;
	}

	public void setOvers_per_innings(double overs_per_innings) {
		this.overs_per_innings = overs_per_innings;
	}

	public double getRuns_per_innings() {
		return runs_per_innings;
	}

	public void setRuns_per_innings(double runs_per_innings) {
		this.runs_per_innings = runs_per_innings;
	}
	public double getFall_of_wickets() {
		return fall_of_wickets;
	}

	public void setFall_of_wickets(double fall_of_wickets) {
		this.fall_of_wickets = fall_of_wickets;
	}


	@Override
	public double averageScore() {
		// TODO Auto-generated method stub
		
		return runs_per_innings/innings;
	}

	@Override
	public double averageFallOfWickets() {
		// TODO Auto-generated method stub
		return fall_of_wickets/overs_per_innings ;
	}

	
	@Override
	public double calcStrikeRate() {
		// TODO Auto-generated method stub
		return (runs_per_innings / overs_per_innings ) * 100;
	}
	public String toString(){
		
		return 
	}
}
