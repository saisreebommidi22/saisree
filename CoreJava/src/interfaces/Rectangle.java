package interfaces;

public class Rectangle implements Shape, Perimeter {

	private double length;
	private double breadth;

	Rectangle() {

	}

	public Rectangle(double length, double breadth) {

		this.length = length;
		this.breadth = breadth;
	}

	public double getLength() {

		return length;
	}

	public void setLength(double length) {

		this.length = length;
	}

	public double getBreadth() {

		return breadth;
	}

	public void setBreadth(double breadth) {

		this.breadth = breadth;
	}

	@Override
	public double calArea() {
		// TODO Auto-generated method stub
		return length * breadth;
	}

	@Override
	public double calcPerimeter() {
		//TODO Auto-generated method stub
	return 2 * (length * breadth);
	}

	public String toString() {

		return "Rectangle [ length =" + getLength() + "breadth =" + getBreadth() + "Area = " + calArea() + "Perimeter"
				+ calcPerimeter() + "]";
	}

}
