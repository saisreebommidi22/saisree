package interfaces;

public class Circle implements Shape {

	private float radius;

	Circle() {

	}

	public Circle(float radius) {

		this.radius = radius;
	}

	public float getRadius() {

		return radius;
	}

	public void setRadius(float radius) {

		this.radius = radius;
	}

	@Override
	public double calArea() {
		// TODO Auto-generated method stub
		return 3.14 * radius * radius;

	}

	public String toString() {

		return "Circle [ radius = " + getRadius() + "Area = " + calArea() + "]";
	}

}
