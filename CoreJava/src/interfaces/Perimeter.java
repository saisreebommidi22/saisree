package interfaces;

public interface Perimeter {
	
	public double calcPerimeter();

}
