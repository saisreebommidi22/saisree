package collections;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class ListOfEmployees {

	public void getEmployeeList() {
		// TODO Auto-generated constructor stub
		List<Employee> empList = new ArrayList<Employee>();

		Employee e1 = new Employee("Saisree", 21000);
		Employee e2 = new Employee("Pooja", 16000);
		Employee e3 = new Employee("Prasana", 23000);
		Employee e4 = new Employee("Sree", 18000);
		Employee e5 = new Employee("Sai", 20000);

		empList.add(e1);
		empList.add(e2);
		empList.add(e3);
		empList.add(e4);
		empList.add(e5);

		System.out.println("EmployeeList");
		for (Employee emp : empList) {
			System.out.println(emp);
		}
		
		ArrayList<Employee> FreshersList = new ArrayList<Employee>();
		
		Iterator<Employee> it = empList.iterator();
		
		while(it.hasNext()){
			Employee emp =it.next();
			if(emp.getSalary()<20000){
				
				FreshersList.add(emp);
				it.remove();
			}
					

		}
		System.out.println("===========================================");
		Collections.sort(empList);
		for (Employee emp : empList) {
			System.out.println(emp);
		}
		System.out.println("===========================================");
		
		System.out.println("Freshers List");
		Collections.sort(FreshersList);
		for (Employee emp : FreshersList) {
			System.out.println(emp);
		}
		
	}
}
