package collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainEmployeeSort {

	List<Employee> empList = new ArrayList<Employee>();

	Employee e1 = new Employee("Saisree", 21000);
	Employee e2 = new Employee("Pooja", 20000);
	Employee e3 = new Employee("Prasana", 23000);
	Employee e4 = new Employee("Sree", 18000);
	Employee e5 = new Employee("Sai", 20000);

	empList.add(e1);
	empList.add(e2);
	empList.add(e3);
	empList.add(e4);
	empList.add(e5);

	System.out.println("EmployeeList");
	for (Employee emp : empList) {
		System.out.println(emp);
	}

	ArrayList<Employee> FreshersList = new ArrayList<Employee>();
	
	Iterator<Employee> it = empList.iterator();
	
	while(it.hasNext()){
		Employee emp =it.next();
		if(emp.getSalary()<20000){
			
			FreshersList.add(emp);
			it.remove();
		}
				

	}
	System.out.println("===========================================");
	System.out.println("employeeList");
	for (Employee emp : empList) {
		System.out.println(emp);
	}

}
}

}
