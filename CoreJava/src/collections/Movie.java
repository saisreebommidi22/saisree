package collections;

public class Movie implements Comparable<Movie> {

	private String name;
	private String director_name;
	private int duration;
	private float rating;

	Movie() {
	}

	public Movie(String name, String director_name, int duration, float rating) {

		this.name = name;
		this.director_name = director_name;
		this.duration = duration;
		this.rating = rating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirector_name() {
		return director_name;
	}

	public void setDirector_name(String director_name) {
		this.director_name = director_name;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String toString() {
		return "Movie [name=" + name + ", director_name=" + director_name + ", duration=" + duration + ", rating="
				+ rating + "]";
	}

	@Override
	public int compareTo(Movie movie) {
		// TODO Auto-generated method stub
		return 0;
	}

}
