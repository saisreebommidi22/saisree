package collections;

import java.util.*;

public class MainMovie {

	public static void main(String args[]) {

		List<Movie> set1 = new ArrayList<Movie>();

		Movie m1 = new Movie("Bhahubali", "Rajamouli", 2, 4.5f);
		Movie m2 = new Movie("Maharshi", "Vamsi", 6, 2.5f);
		Movie m3 = new Movie("ArjunReddy", "Sandeep", 3, 5f);

		set1.add(m1);
		set1.add(m2);
		set1.add(m3);

		for (Movie m : set1){
			System.out.println(m);
			
		}
		System.out.println("====================================");
		System.out.println("Sorting by movie name");
		Collections.sort(set1, new MovieNameSorter());
		
		for (Movie m : set1){
			System.out.println(m);
			
		}
		System.out.println("====================================");
		System.out.println("Sorting by director name");
		Collections.sort(set1, new DirectorNameSorter());
		
		for (Movie m : set1){
			System.out.println(m);
			
		}
		System.out.println("====================================");
		System.out.println("Sorting by duration");
		Collections.sort(set1, new DurationSorter());
		
		for (Movie m : set1){
			System.out.println(m);
			
		}
		System.out.println("====================================");
		System.out.println("Sorting by rating");
		Collections.sort(set1, new RatingSorter());
		
		for (Movie m : set1){
			System.out.println(m);
			
		}
		
		


	}

}
