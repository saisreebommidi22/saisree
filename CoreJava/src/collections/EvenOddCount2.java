package collections;

import java.util.ArrayList;
import java.util.List;

public class EvenOddCount2 {
	
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		
		System.out.println(list);
		getCount(list);
		
		
	}
	public static void getCount(List<Integer> list)
	{
		int evenCount=0;
		int oddCount=0;
		
		for(int i: list){
			
			if(i%2==0)
				evenCount++;
			else
				oddCount++;
		}
		System.out.println(evenCount);
		System.out.println(oddCount);
		
	}

}
