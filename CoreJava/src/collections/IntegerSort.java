package collections;

import java.util.*;

public class IntegerSort {
	
	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		
		list.add(93);
		list.add(10);
		list.add(23);
		list.add(45);
		list.add(35);
		
		System.out.println(list);
		
		Collections.sort(list);
		
		System.out.println(list);
	}

}
