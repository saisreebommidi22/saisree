package collections;

import java.util.Comparator;

public class DirectorNameSorter implements Comparator<Movie> {

	@Override
	public int compare(Movie m1, Movie m2) {
		// TODO Auto-generated method stub
		return m1.getDirector_name().compareTo(m2.getDirector_name());
	}

}
