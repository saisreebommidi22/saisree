package collections;

public class COJ_65_CabCustomer {
	
	private int custId=101;
	private int idGenerator;
	private String customerName;
	private String pickUpLocation;
	private String dropLocation;
	private int distance;
	private long phone;
	
	COJ_65_CabCustomer(){
		
	}

	public COJ_65_CabCustomer(String customerName, String pickUpLocation, String dropLocation, int distance,
			long phone) {
		super();
		this.custId = idGenerator++;
		this.customerName = customerName;
		this.pickUpLocation = pickUpLocation;
		this.dropLocation = dropLocation;
		this.distance = distance;
		this.phone = phone;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPickUpLocation() {
		return pickUpLocation;
	}

	public void setPickUpLocation(String pickUpLocation) {
		this.pickUpLocation = pickUpLocation;
	}

	public String getDropLocation() {
		return dropLocation;
	}

	public void setDropLocation(String dropLocation) {
		this.dropLocation = dropLocation;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "COJ_65_CabCustomer [custId=" + custId + ", customerName=" + customerName + ", pickUpLocation="
				+ pickUpLocation + ", dropLocation=" + dropLocation + ", distance=" + distance + ", phone=" + phone
				+ "]";
	}
	
	

}
