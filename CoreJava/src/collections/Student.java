package collections;

public class Student {
	
	static int idGenerator=101;
	private int id;
	private String name;
	private double subject1;
	private double subject2;
	private double subject3;
	
	Student(){
		
	}

	public Student(String name, double subject1, double subject2, double subject3) {
		
		super();
		this.id = idGenerator++;
		this.name = name;
		this.subject1 = subject1;
		this.subject2 = subject2;
		this.subject3 = subject3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSubject1() {
		return subject1;
	}

	public void setSubject1(double subject1) {
		this.subject1 = subject1;
	}

	public double getSubject2() {
		return subject2;
	}

	public void setSubject2(double subject2) {
		this.subject2 = subject2;
	}
	
	public double getPercentage(){
		
		return (subject1+subject2+subject3)/3;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", subject1=" + subject1 + ", subject2=" + subject2
				+ ", subject3=" + subject3 + "Percentage="+getPercentage()+ "]";
	}
	
	

}
