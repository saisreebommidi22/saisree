package collections;

public class Employee implements Comparable<Employee> {
	
	private int id;
	private String name;
	private double salary;
	static int idGenerator;
	
	Employee(){
		
	}

	public Employee(String name, double salary) {
		this.id = idGenerator++;
		this.name = name;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}

	@Override
	public int compareTo(Employee emp) {
		
		if(salary > emp.getSalary())
			return 1;
		else if(salary < emp.getSalary())
			return -1;
		// TODO Auto-generated method stub
		return 0;
	}
	

}
