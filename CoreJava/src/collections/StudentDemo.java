package collections;

import java.util.ArrayList;
import java.util.List;

public class StudentDemo {

	public static void main(String[] args) {

		List<Student> list = new ArrayList<Student>();

		Student s1 = new Student("Saisree", 70, 70, 70);
		Student s2 = new Student("Bharath", 90, 90, 90);
		Student s3 = new Student("Sravani", 90, 98, 92);

		list.add(s1);
		list.add(s2);
		list.add(s3);

		for (int i = 0; i < list.size(); i++) {

			System.out.println(list.get(i));
		}
	}

}
