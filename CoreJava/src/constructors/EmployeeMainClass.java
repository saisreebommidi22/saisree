package constructors;

public class EmployeeMainClass {
	
	public static void main(String[] args) {
		
		Employee emp = new Employee("Sai","sree",10000);
		
		System.out.println(emp);
		System.out.println("Raised Salary"+emp.raiseSalary(30));
		Employee emp1 = new Employee("Navya","sree",10000);
		
		System.out.println(emp1);
		System.out.println("Raised Salary"+emp1.raiseSalary(40));
		
	}

}
