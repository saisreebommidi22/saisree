package constructors;

public class Player {

	private int id;

	private String name;

	static int idGenerator = 101;

	Player(){

	}

	public Player(String name) {

		this.name = name;
		id = idGenerator++;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getName() {

		return name;
	}
	public String toString() {

		return "Player [ id = " + id + ",Name = " + name + " ]";

	}

}