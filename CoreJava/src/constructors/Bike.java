package constructors;

public class Bike extends Vehicle {
	
	public Bike(String brand,String model, String registration){
		
		super(brand,model,registration);
	}

	public void followSafety() {
		
		super.followSafety();
		System.out.println("Be Careful while driving bike");
	}

	public void drive() {
		
		super.drive();

		System.out.println("While Driving a bike wear helmet");
	}
	public String toString(){
		
		return "Bike [ "+super.toString();
	}

}
