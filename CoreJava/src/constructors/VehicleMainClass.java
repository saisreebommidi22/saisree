package constructors;

public class VehicleMainClass {

	public static void main(String[] args) {

		Bike b = new Bike("RoyalEnfield", "1990", "Ap16h3244");

		Car c = new Car("Swift", "2000", "AP16453236");

		System.out.println(b);

		b.followSafety();
		b.drive();

		System.out.println(c);

		c.followSafety();
		c.drive();

	}

}
