package constructors;

public class Car extends Vehicle {

	public Car(String brand, String model, String registration) {

		super(brand, model, registration);
	}

	public void followSafety() {
		
		super.followSafety();
		System.out.println("Be Careful while driving car");
	}

	public void drive() {
		super.drive();
		System.out.println("Keep your seatbelt while driving a car");
	}

	public String toString() {

		return "Car [ " + super.toString();
	}

}
