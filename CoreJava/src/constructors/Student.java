package constructors;

public class Student {
	
	public Student(){
		
		System.out.println("Default Constructor");
	}
	
	public Student(int i){
		
		System.out.println("Parameterized Constructor");
	}


}
