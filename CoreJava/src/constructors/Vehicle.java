package constructors;

public class Vehicle {
	
	private String brand;
	private String model;
	private String registration;
	
	Vehicle(){
		
	}
	public Vehicle(String brand, String model, String registration){
		
		this.brand = brand;
		this.model = model;
		this.registration = registration;
	}
	public String getBrand() {
		
		return brand;
	}
	public void setBrand(String brand) {
		
		this.brand = brand;
	}
	public String getModel() {
		
		return model;
	}
	public void setModel(String model) {
		
		this.model = model;
	}
	public String getRegistration() {
		
		return registration;
	}
	public void setRegistration(String registration) {
		
		this.registration = registration;
	}
	public void followSafety(){
		
		System.out.println("Be careful");
	}
	public void drive(){
		
		System.out.println("Drive Safe");
		
	}
	public String toString(){
		
		return "Brand = " +brand+" Model ="+model+" Registration = "+registration;
	}

}
