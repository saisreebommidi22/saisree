package constructors;

public class Employee{

	private int id;
	static int getGenerator = 101;
	private String firstName;
	private String lastName;
	private int salary;

	public Employee(String firstName, String lastName, int salary) {

		this.id = getGenerator++;
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;

	}

	public void setfirstName(String firstName) {

		this.firstName = firstName;
	}

	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	public void setSalary(int salary) {

		this.salary = salary;
	}

	public String getFirstName() {

		return firstName;
	}

	public String getLastName() {

		return lastName;
	}

	public int getSalary() {

		return salary;
	}

	public String getName() {

		return firstName.concat(lastName);
	}

	public int getAnnualSalary() {

		return salary * 12;
	}

	public int raiseSalary(int percentage) {

		return (salary * percentage) / 100 + salary;
	}

	public String toString() {

		return "EMPLOYEE [ id = " + id + " ,Name = " + getName() + ",Salary = " + getSalary()+ ",AnnualSalary = "
				+ getAnnualSalary()+"]";
	}

}
