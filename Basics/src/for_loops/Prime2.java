package for_loops;

import java.util.Scanner;

public class Prime2 {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int num =sc.nextInt();

		if(isPrimeNumber(num))
			System.out.println(num +" Is a prime Number");

		else 
			System.out.println(num + " Is not a prime number");

	}
	public static boolean isPrimeNumber(int num){

		for(int i = 2 ; i < num ; i++){

			if( num % i == 0)

				return false;
		}
		return true;
	}

}
