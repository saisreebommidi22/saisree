package for_loops;

import java.util.Scanner;

public class Table_Generation {
	
	public static void main(String[] args){
		
		Scanner sc  = new Scanner(System.in);
		
		int num = sc.nextInt();
		getTable(num);
		
	}
	public static void getTable(int num){
		
		int res;
		
		for(int i =1; i<=10; i++ ){
			
			res = num * i;
			
			System.out.println(num+" * "+i+ " = "+res);
		}
	}

}
