package for_loops;

import java.util.Scanner;

public class Second_Max {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		System.out.println(getMaxSecondDigit(num));
		
		
	}

	public static int getMaxSecondDigit(int num) {

		int digit = 0;
		int max = 0, secondMax = 0;
		for (; num > 0;num/=10) {

			digit = num % 10;

			if (digit > max) {

				secondMax = max;
				max = digit;
			}
			else if (digit < max && digit > secondMax) 
				secondMax = digit;
		}

			return secondMax;
	}
}