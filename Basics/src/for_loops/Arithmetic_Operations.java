package for_loops;

import java.util.Scanner;

public class Arithmetic_Operations {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int number1 = sc.nextInt();
		int number2 = sc.nextInt();
		
		System.out.println(getOperations(number1,number2));
	}
	public static String getOperations(int n1 , int n2){

		Scanner sc = new Scanner(System.in);
		char ch;
		String result = "";
		String finalResult = "";
		do{

			System.out.println("Menu :\n1.Addition\n2.Subtraction\n" + "3.Multiplication\n4.Divison\n" +"5.Remainder\n6.Power");

			System.out.print("Enter option : ");
			int option = sc.nextInt();

			switch (option) {
			case 1:
				result = "Addition of two numbers : " + (n1+n2);
				finalResult +="\n "+result ;
				break;
				
			case 2:
				if(n1> n2)
					result = "Subtraction of two numbers : " +(n1-n2);
				else
					result ="Subtraction of two numbers : " +(n2-n1);
				finalResult += "\n"+result;
				break;
			case 3:
				result = "Multiplication of two numbers : " +(n1*n2);
				finalResult += "\n"+result;
				break;
			case 4:
				if(n1>n2)
					result = "Divison of two numbers : " +(n1/n2);
				else 
					result = "Divison of two numbers : " +(n2/n1);
				finalResult += "\n"+result;
				break;
			case 5:
				if(n1>n2)
					result = "Remainder of two numbers : "+(n1%n2);
				else
					result = "Remainder of two numbers : "+(n2%n1);
				finalResult += "\n"+result;
				break;
			case 6:
				int i = 1;
				int power = 1;
				while(i <= n2){
					power *= n1;
					i++;
				}
				result = n1 + "Power of"+ n2 + " : "+power;
				finalResult += "\n"+result;
				break;
			default:
				System.out.println("Invalid Option");
		}
			System.out.println("Do you want to continue or not (y/n)?");
			ch = sc.next().charAt(0);
		}
		while(ch=='y');
			return finalResult;

}
}