package for_loops;

import java.util.Scanner;

public class Factorial {
	
	public static void main(String[] args){
		
		Scanner sc =new Scanner(System.in);
		
		int num = sc.nextInt();
		
		System.out.println("Factorial of a given number is :"+getFactorial(num));
		
	}
	public static int getFactorial(int num){
		
		int factorial =1;
		
		for(int i = num; i>=1; i--){
			
			factorial = factorial * i;
			
		}
		return factorial;
	}

}
