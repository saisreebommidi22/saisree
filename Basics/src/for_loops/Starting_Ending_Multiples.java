package for_loops;

import java.util.Scanner;

public class Starting_Ending_Multiples {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int starting_number = sc.nextInt();
		int ending_number =sc.nextInt();
		
		System.out.println(+getSum(starting_number,ending_number));
	}
	public static int getSum(int start, int end){
		
		int sum =0;
		
		for(int i =start; i<= end; i++){
			
			if(i%3==0 || i%5==0){
				sum=sum+i;
			}
		}
		return sum;
	}

}
