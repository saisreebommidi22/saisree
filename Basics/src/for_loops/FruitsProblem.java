package for_loops;

public class FruitsProblem {

	public static void main(String[] args){
		
		getCombination();		
		
	}
	public static void getCombination(){

		for( int banana=1 ; banana<=100 ; banana++){

			for( int oranges=1 ; oranges<=100 ; oranges++){

				for(int apples=1 ; apples<=100 ; apples++){

					if( (banana+oranges+apples)==100){
						
						double totalCost = (banana * 0.5) +(oranges * 1)+(apples *5);
						if(totalCost == 100){
							
							System.out.print("bananas :  "+ banana);
							System.out.print(" oranges :  "+ oranges);
							System.out.print(" apples : "+ apples);
							System.out.println(" Cost:" +totalCost);
						
						}
					}
				}
			}
		}
	}
}
