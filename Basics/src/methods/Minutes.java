package methods;

import java.util.Scanner;

public class Minutes {
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		int minutes = sc.nextInt();
		int years = (minutes/(24*60*365));
		int days = minutes/(24*60)%365;
		int hours =(minutes/60)%24;
		int min = minutes%24;
		System.out.println("Given minutes has "+years+" years "+days+" days "+hours+" hours and "+min+" minutes"); 
		
	}

}
