package methods;

import java.util.Scanner;

public class Average_Three {
	
	public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		int num3 = sc.nextInt();
		float average = getAverage(num1,num2,num3);
		System.out.println("Average :"+average);
	}
	public static float getAverage(int num1,int num2,int num3){
		
		float average=(num1 + num2 + num3)/3;
		return average;
		
	}

}
