package methods;

import java.util.Scanner;

public class ArithmeticOperations {
	
	public static void main(String args[]){
		
		Scanner sc=new Scanner(System.in);
		
		int num1=sc.nextInt();
		int num2=sc.nextInt();
		
		int sum =getAddition(num1,num2);
		int diff =getDifference(num1,num2);
		int product=getProduct(num1,num2);
		int divison =getDivison(num1,num2);
		
		System.out.println("Addition :"+sum);
		System.out.println("Subtraction :"+diff);
		System.out.println("Multiplication :"+product);
		System.out.println("Divison :"+divison);
		
	}

	public static int getAddition(int num1,int num2){
		
		int sum = num1 + num2;
		return sum;
		
	}
	public static int getDifference(int num1,int num2){
		
		int diff = num1 - num2;
		return diff;
	}
	public static int getProduct(int num1,int num2){
		
		int product = num1 * num2;
		return product;
		
	}
	public static int getDivison(int num1,int num2){
		
		int divison = num1 / num2;
		return divison;
	}
}
