package methods;

import java.util.Scanner;

public class AreaPerimeter_Circle {
	
	public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int radius = sc.nextInt();
		double area = area(radius);
		double perimeter= perimeter(radius);
		
		System.out.println("Area : "+area);
		System.out.println("Perimeter : "+perimeter);
		
	}
	public static double area(int radius){
		
		double area = 3.14 * radius * radius;
		return area;
	}
	
	public static double perimeter( int radius){
		
		double perimeter = 2 * 3.14 * radius;
		return perimeter;
	}

}
