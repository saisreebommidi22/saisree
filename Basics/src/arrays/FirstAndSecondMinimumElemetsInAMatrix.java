package arrays;

import java.util.Scanner;

public class FirstAndSecondMinimumElemetsInAMatrix {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row size : ");
		int row_size = sc.nextInt();

		System.out.println("Enter column size :");
		int column_size = sc.nextInt();

		int matrix[][] = new int[row_size][column_size];

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				matrix[i][j] = sc.nextInt();
			}
		}

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		getFirstAndSecondMinimumElementsOfMatrix(matrix);

	}

	public static void getFirstAndSecondMinimumElementsOfMatrix(int[][] matrix) {

		int minimum_Number = matrix[0][0];
		int minimum_Number2 = matrix[0][0];
		int temp = 0;
		for (int i = 0; i < matrix.length; i++) {

			for (int j = 0; j < matrix.length; j++) {

				if (minimum_Number < matrix[i][j]) {

					temp = minimum_Number;
					minimum_Number = minimum_Number2;
					minimum_Number2 = temp;
				} else
					minimum_Number = matrix[i][j];
			}
		}
		System.out.println("First Minimum element of the matrix is :" + minimum_Number);

		System.out.println("Second Minimum element of the matrix is :" + minimum_Number2);
	}
}
