package arrays;

import java.util.Scanner;

public class FirstAndSecondMinimumElements{
	
		public static void main(String[] args) {

			Scanner sc = new Scanner(System.in);

			int n = 6;

			int arr[] = new int[n];

			for (int i = 0; i < n; i++) {
				arr[i] = sc.nextInt();

			}
			getMin(n, arr);
		}

		public static void getMin(int n, int[] arr) { // 1+3 = 4
			
			int temp;
			
			for (int i = 0; i < n; i++) {
				
			
				for (int j = i + 1; j < n; j++) {
					
					if (arr[i] > arr[j]) {
						
						temp = arr[i];
						arr[i] = arr[j];
						arr[j] = temp;
					}
				}
			}

			System.out.println(arr[0] + "+" + arr[1] + " = " + (arr[0] + arr[1]));

		}
}