package arrays;

import java.util.Scanner;

public class MaxElementInAArray {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row size : ");
		int row_size = sc.nextInt();

		System.out.println("Enter column size :");
		int column_size = sc.nextInt();

		int matrix[][] = new int[row_size][column_size];

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				matrix[i][j] = sc.nextInt();
			}
		}

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		getMaximumElementOfArray(matrix);

	}

	public static void getMaximumElementOfArray(int[][] matrix) {

		int max = matrix[0][0];

		for (int i = 0; i < matrix.length; i++) {

			for (int j = 0; j < matrix.length; j++) {

				if (max < matrix[i][j])

					max = matrix[i][j];
			}
		}
		System.out.println("Maximum element of the matrix is :"+max);
	}
}
