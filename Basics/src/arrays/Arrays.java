package arrays;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {

		System.out.println ("Average is "+getAverage());

	}

	public static int getAverage() {

		Scanner sc = new Scanner(System.in);

		int[] arr = new int[5];
		int sum = 0;
		System.out.println("Enter array inputs: ");

		for (int i = 0; i < 5; i++) {

			arr[i] = sc.nextInt();
			sum += arr[i];
		}
		return (sum / 5);
	}

}