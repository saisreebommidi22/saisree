package arrays;


import java.util.*;

public class Fruits {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n;
		
		n = sc.nextInt();
		
		int arr[] = new int[n];
		
		int res[] = getNoOfAlmonds(arr, n);
		
		int sum = 0;
		
		for (int i = 0; i < n; i++) {
			sum = sum + arr[i];
		}
		System.out.println("total no of almonds he has to buy is" + sum);

	}

	public static int[] getNoOfAlmonds(int arr[], int n) {
		
		arr[0] = 2;
		arr[1] = 3;
		arr[2] = 5;
		
		for (int i = 3; i < n; i++) {
			arr[i] = arr[i - 1] + arr[i - 2] + arr[i - 3];
		}
		return arr;
	}

}
