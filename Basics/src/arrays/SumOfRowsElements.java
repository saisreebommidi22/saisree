package arrays;

import java.util.Scanner;

public class SumOfRowsElements {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row size : ");
		int row_size = sc.nextInt();

		System.out.println("Enter column size :");
		int column_size = sc.nextInt();

		int matrix[][] = new int[row_size][column_size];

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				matrix[i][j] = sc.nextInt();
			}
		}
		System.out.println(" Array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				System.out.print(matrix[i][j] + " ");
			}
			System.out.println(" ");
		}
		getSumOfRows(matrix);
	}

	public static void getSumOfRows(int[][] matrix) {

		for (int i = 0; i < matrix.length; i++) {

			int sum = 0;

			for (int j = 0; j < matrix.length; j++) {

				sum = sum + matrix[i][j];
			}
			System.out.println(sum);

		}

	}
}
