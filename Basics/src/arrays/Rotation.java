package arrays;

public class Rotation {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5 };

		int n = 2;

		rotateArr(arr, n);

	}

	public static void rotateArr(int arr[], int n) {

		for (int i = 1; i <= n; i++) {

			int last = arr[arr.length - 1];

			for (int j = arr.length - 1; j > 0; j--) {

				arr[j] = arr[j - 1];
			}
			arr[0] = last;

		}
		for (int k = 0; k < arr.length; k++) {
			System.out.println(arr[k] + " ");
		}

	}
}
