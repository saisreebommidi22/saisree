package arrays;

import java.util.Scanner;

public class PrintingIdentityMatrix {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row size : ");
		int row_size = sc.nextInt();

		System.out.println("Enter column size :");
		int column_size = sc.nextInt();

		int identityMatrix[][] = new int[row_size][column_size];

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				identityMatrix[i][j] = sc.nextInt();
			}
		}

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				System.out.print(identityMatrix[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println(isIdentityMatrix(row_size,column_size,identityMatrix));
	}

	public static boolean isIdentityMatrix( int row_size, int column_size, int[][] matrix) {

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				if (i != j) {
					if (matrix[i][j] != 0) 
						return false;
				}
				if (i == j) {
					if (matrix[i][j] != 1) {
						return false;
					}
				}
			}
		}
		return true;
	}
}
