package arrays;

import java.util.Scanner;

public class LinearSearch {

	public static void main(String[] args) {

		int index = doLinearSearch();

		if (index != -1)
			System.out.println("Key value is found at " + index + " location");
		else
			System.out.println("Key value not found");

	}

	public static int doLinearSearch() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size :");

		int size = sc.nextInt();

		int[] arr = new int[size];

		System.out.println(" Enter array elements : ");

		for (int i = 0; i < size; i++) {

			arr[i] = sc.nextInt();
		}
		System.out.println("Enter key value :");

		int key = sc.nextInt();

		for (int i = 0; i < size; i++) {

			if (key == arr[i])

				return i;
		}
		return -1;

	}

}
