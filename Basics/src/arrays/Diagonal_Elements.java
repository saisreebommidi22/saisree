package arrays;

import java.util.Scanner;

public class Diagonal_Elements {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row size : ");
		int row_size = sc.nextInt();

		System.out.println("Enter column size :");
		int column_size = sc.nextInt();

		int diagonalMatrix[][] = new int[row_size][column_size];

		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				diagonalMatrix[i][j] = sc.nextInt();
			}
		}
		System.out.println("Enter array elements :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				System.out.print(diagonalMatrix[i][j] + " ");
			}
			System.out.println();
		}
		getDiagonalMatrix(row_size, column_size, diagonalMatrix);
	}

	public static void getDiagonalMatrix(int row_size, int column_size, int[][] diagonalMatrix) {

		System.out.println("Left Diagonal :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				if (i == j)
					System.out.println(diagonalMatrix[i][j]+" ");
			}
		}
		System.out.println("Right Diagonal :");

		for (int i = 0; i < row_size; i++) {

			for (int j = 0; j < column_size; j++) {

				if ((i + j) == diagonalMatrix.length - 1)
					System.out.println(diagonalMatrix[i][j]+" ");
			}
		}

	}

}