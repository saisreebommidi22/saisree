package arrays;

import java.util.Scanner;

public class Sorting_An_Array_In_AscendingOrder {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int sizeOfArray = sc.nextInt();

		int arr[] = new int[sizeOfArray];

		for (int i = 0; i < sizeOfArray; i++) {

			arr[i] = sc.nextInt();
		}
		getSortedArray(sizeOfArray, arr);
	}

	public static void getSortedArray(int sizeOfArray, int arr[]) {

		int temp;

		for (int i = 0; i < sizeOfArray; i++) {

			for (int j = i + 1; j < sizeOfArray; j++) {

				if (arr[i] > arr[j]) {

					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}

		}

		for (int i = 0; i < sizeOfArray; i++) {

			System.out.println(arr[i] + " ");
		}
	}

}
