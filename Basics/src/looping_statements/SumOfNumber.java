package looping_statements;

import java.util.Scanner;

public class SumOfNumber {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		getSum(num);
	}
	public static void getSum(int num){
		
		
		while(num>0){
			
			int remainder =  num%10;
			System.out.println("Digit is :"+remainder);
			num = num/10;
		}

	}

}
