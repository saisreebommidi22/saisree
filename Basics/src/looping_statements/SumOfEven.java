package looping_statements;

import java.util.Scanner;

public class SumOfEven {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		System.out.println("Sum = " +getEvenSum(num));
	}
	public static int getEvenSum(int num){
		
		int i=1,sum = 0;
		
		while(i<=num){
			
			if(i%2 == 0){
				 
				sum = sum +i ;
			}
			i++;
		}
		return sum;
	}

}
