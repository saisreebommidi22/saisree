package looping_statements;

import java.util.Scanner;

public class ReverseOfNumber {
	
	public static void main( String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		System.out.println(getReverse(num));
		
		
	}
	public static String getReverse(int num){
		
		int sum = 0;
		int temp =num;
		String res="";
		
		while(temp>0){
			
			int remainder =temp%10;
			sum = sum*10+remainder;
			temp=temp/10;
		}
			
			res ="Given Number :"+num+"\nReverse of given number is "+sum;
		return res;
	}

}
