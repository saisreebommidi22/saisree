package looping_statements;

import java.util.Scanner;

public class SumOfNaturalNum {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		System.out.println(getSum(num));
	}
	public static String getSum(int num){
		
		int sum =0;
		String res ="";
		
		while(num>0){
			
			sum = sum + num;
			 num--;
		}
		
		res ="Sum is "+sum;
		return res;
	}
	

}
