package looping_statements;

import java.util.Scanner;

public class Count_Even_Odd {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		int num =sc.nextInt();

		getCount(num);
	}

	public static void getCount(int num){//Even number of digits  : 1

		int remainder;
		int j =0;
		int i=0;
		while(num>0){

			remainder =num %10;

			if(remainder%2==0)

				i++;
			else
				j++;

			num = num/10;

		}
		System.out.println("Even number of digits  : "+i+"\nOdd number of digits  : "+j);
	}
}

