package wrapperClasses;

import java.util.Scanner;

public class CaseConversion {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String str = sc.nextLine();
		
		getConvertedCase(str);

	}

	public static void getConvertedCase(String str) {

		String convertCase = "";

		for (int i = 0; i < str.length(); i++) {

			char c = str.charAt(i);

			if (Character.isLowerCase(c))

				convertCase += Character.toUpperCase(c);

			else if (Character.isUpperCase(c))

				convertCase += Character.toLowerCase(c);
			else

				convertCase += c;

		}
		System.out.println(convertCase);
	}
}
