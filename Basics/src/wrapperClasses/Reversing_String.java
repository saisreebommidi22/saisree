package wrapperClasses;

import java.util.Scanner;

public class Reversing_String {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String str = sc.nextLine();

		System.out.println(getReversedString(str));
	}

	public static String[] splitingString(String str) {

		String[] splittedString = str.split(" ");

		return splittedString;
	}

	public static String getReversedString(String str) {

		String[] s = splitingString(str);

		String reverseString = "";

		for (int i = 0; i < s.length; i++) {
			String word = s[i];

			for (int j = word.length() - 1; j >= 0; j--) {

				reverseString += word.charAt(j);
			}
			reverseString += " ";
		}
		return reverseString;

	}
}
