package wrapperClasses;

import java.util.Scanner;

public class Reversing_String2 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String str = sc.nextLine();

		System.out.println(getWordReverse(str));

	}

	public static String getWordReverse(String str) {

		String revstr = "";
		String[] s = str.split(" ");

		for (int i = 0; i < s.length; i++) {
			String word = s[i];

			for (int j = word.length() - 1; j >= 0; j--) {
				revstr += word.charAt(j);

			}
			revstr += " ";

		}
		return revstr;

	}

}
