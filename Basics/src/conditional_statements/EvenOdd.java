package conditional_statements;

import java.util.Scanner;

public class EvenOdd {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int number = sc.nextInt();
		
		System.out.println(evenodd(number));
	}

	public static String evenodd(int number) {
		
		String res;

		if ((number % 2) == 0) {
			
			 res = "Given number " + number + " is even.";
			
		}

		else {
			
			res = "Given number " + number + " is even.";
		}
		return res;
	}

}
