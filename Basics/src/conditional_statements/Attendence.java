package conditional_statements;

import java.util.Scanner;

public class Attendence {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		float classes_attended = sc.nextInt();
		float classes_held = sc.nextInt();

		getPercentage(classes_attended, classes_held);

		System.out.println(getPercentage(classes_attended,classes_held));

	}

	public static String getPercentage(float classes_attended, float classes_held) {

		float percentage;
		String res = "";

		percentage = (classes_attended * 100/ classes_held) ;

		if (percentage >= 75) {

			res = "Percentage is " + percentage + " \nAllowed for exam";
			return res;
		} 
		else {
			res = "Percentage is " + percentage + " \nNot allowed for exam";
		return res;
		}
	}
}
