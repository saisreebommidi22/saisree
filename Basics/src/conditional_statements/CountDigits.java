package conditional_statements;

import java.util.Scanner;

public class CountDigits {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int number = sc.nextInt();
		
		System.out.println(countDigits(number));
	}
	public static String countDigits(int number){
		
		String res;
		
		if(number>9999 && number<=99999){
			
			res = "Given number is a five digit number";
			
		}
		else{
			
			res = "Given number is not a five digit number";
		}
			return res;
	}

}
