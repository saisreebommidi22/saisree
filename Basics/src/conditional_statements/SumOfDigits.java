package conditional_statements;

public class SumOfDigits {

	public static void main(String[] args){
	 int num = 79;
     System.out.println(getSumOfDigits(num));
 }

	public static int getSumOfDigits(int num) {

		int sum, quotient, remainder;

		if (num > 10 && num <= 99) {

			quotient = num / 10;
			remainder = num % 10;
			sum = quotient + remainder;
			return sum;
		} else {

			return 0;
		}

	}

}
