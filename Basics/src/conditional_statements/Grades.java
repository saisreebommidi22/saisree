package conditional_statements;

import java.util.Scanner;

public class Grades {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int subject1 = sc.nextInt();
		int subject2 = sc.nextInt();
		int subject3 = sc.nextInt();
		
		System.out.println(getGrade(subject1,subject2,subject3));

	}

	public static String getGrade(int num1, int num2, int num3) {

		String res = "";
		int percentage = (num1 + num2 + num3)*100/300;

		if (percentage >= 90) {

			res = "Grade = A";

		} else if (percentage >= 70 && percentage < 90) {

			res = "Grade = B";
		} else if (percentage >= 50 && percentage < 70) {

			res = "Grade = C";
		} else {

			res = "Grade = F";
		}
		return res;
	}
}
