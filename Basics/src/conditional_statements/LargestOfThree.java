package conditional_statements;

import java.util.Scanner;

public class LargestOfThree {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		int num3 = sc.nextInt();
		

		
		System.out.println(largestOfThree(num1,num2,num3)+" is greatest number");
	}
	public static int largestOfThree(int num1, int num2, int num3){
		
		int res;
		
		if(num1>num2 && num1>num3){
			
			res =num1;
		}
		else if(num2>num1 && num2>num3){
			
			res = num2;
		}
		else{
			
			res = num3;
		}
		return res;
		
	}

}
