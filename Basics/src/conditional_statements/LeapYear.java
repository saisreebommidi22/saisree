package conditional_statements;

import java.util.Scanner;

public class LeapYear {
	
	public static void  main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int year =sc.nextInt();
		
		System.out.println(isLeapYear(year));
	}
	public static Boolean isLeapYear(int year){
		
		boolean b = false;
		
		if( year % 4 == 0){
			b = true;
			if( year % 100 == 0){
				if(year % 400 ==0)
					b = true;
				else 
					b= false;
			}
		}
			
		else
			b = false;
			
		return b;
}
}
