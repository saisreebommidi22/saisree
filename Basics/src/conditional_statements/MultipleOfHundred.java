package conditional_statements;

import java.util.Scanner;

public class MultipleOfHundred {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		
		System.out.println(getNextMultipleOf100(num));

	}

	public static int getNextMultipleOf100(int num) {

		int next_multiple = 0;

		if (num <= 0) {

			return -1;

		} else {
			next_multiple = ((num / 100) + 1) * 100;
			return next_multiple;

		}
	}

}
