public class Palindrome01 {
    public static void main(String[] args) {
        int num = 878;
        System.out.println(isPalindrome(num));
    }

    public static int isPalindrome(int num) {
    	int re = 0;
    	
    	if(num <= 0 )
    		re = -1;
    	
    	else if( num >=100 && num <= 999) {
    		int sum = 0,remainder,temp;
       		temp = num;
    	    	
    			while(temp>0){
    	    		
    	    		remainder = temp % 10;
    	    		sum = sum *10 +remainder;
    	    		temp =temp /10;
    	        }
    			if(num == sum)
    				re = 1;
    			
 		}
    	else if(num < 100 || num > 999)
			re = 0;
    	
    	return re;
    }	
    
}
