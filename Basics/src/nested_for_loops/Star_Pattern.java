package nested_for_loops;

import java.util.Scanner;

public class Star_Pattern {

	public static void main (String[] args){

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();

		System.out.println(getPattern(num));
	}

	public static String getPattern(int num){

		String result="";

		if(num <=0)

			return "-1";

		for(int i=1;i<=num;i++)
		{
			for(int k=num;k>i;k--){

				result=result+"  ";
			}
			for(int j=1;j<=i;j++){

				result=result+"  *";
			}
			result=result+"\n";
		}
		return result;
	}

}
