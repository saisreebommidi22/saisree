package variables;

import java.util.Scanner;

public class Solution {
	
	public static void main(String args[])
	
	{
		Scanner sc = new Scanner(System.in);
		
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		
		int sum = num1+num2;
		int difference= num1-num2;
		
		System.out.println("sum = "+sum);
		System.out.println("difference = "+difference);
		
	}

}
