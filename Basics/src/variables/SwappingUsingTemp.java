package variables;

import java.util.Scanner;

public class SwappingUsingTemp {

public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		
		System.out.println("Before swapping");
		System.out.println("num1 = "+num1);
		System.out.println("num2 = "+num2);
		
		int num3 = num1;
		num1 = num2;
		num2 = num3;
		
		System.out.println("After swapping");
		System.out.println("num1 = "+num1);
		System.out.println("num2 = "+num2);

   }
}
