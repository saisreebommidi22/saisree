package variables;

import java.util.*;

public class Swapping {
	
	public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
		
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		
		System.out.println("Before swapping");
		System.out.println("num1 = "+num1);
		System.out.println("num2 = "+num2);
		
		num1= num1+num2;
		num2 = num1-num2;
		num1 = num1-num2;
		
		System.out.println("After swapping");
		System.out.println("num1 = "+num1);
		System.out.println("num2 = "+num2);
	}

}
