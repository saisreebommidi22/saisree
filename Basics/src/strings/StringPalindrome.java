package strings;

import java.util.Scanner;

public class StringPalindrome {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String str = sc.next();

		isPalindrome(str);
	}

	public static void isPalindrome(String str) { 

		String reverse = "";

		for (int i = str.length() - 1; i >= 0; i--)

			reverse = reverse + str.charAt(i);

		if (str.equalsIgnoreCase(reverse))

			System.out.println(str + " is a Palindrome");
		else

			System.out.println(str + " Not a palindrome");
	}

}