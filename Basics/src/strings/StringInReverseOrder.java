package strings;

import java.util.Scanner;

public class StringInReverseOrder {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		String str = sc.nextLine();
		
		getReverseOfAString(str);
		
	}
	public static void getReverseOfAString(String str){
		
		String reverse = " ";
		
		for(int i = str.length()-1; i >= 0 ;i--){
			
			reverse = reverse + str.charAt(i);
			
		}
		System.out.println("Reverse of a given string "+str+" is "+reverse);
	}
}
