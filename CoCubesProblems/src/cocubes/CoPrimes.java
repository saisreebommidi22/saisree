package cocubes;

import java.util.Scanner;

public class CoPrimes {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int size = sc.nextInt();

		int array[] = new int[size];

		for (int i = 0; i < array.length; i++) {

			array[i] = sc.nextInt();
			
		}
		System.out.println(getCoPrimesCount(array, size));

	}

	public static int getCoPrimesCount(int[] arr, int size) {

		int count = 0;

		for (int i = 0; i < size - 1; i++) {

			for (int j = i + 1; j < size; j++) {

				if (coPrime(arr[i], arr[j])) {
					count++;
				}
			}
		}
		return count;

	}

	private static boolean coPrime(int i, int j) {

		int gcd = 0;
		while (i != 0) {

			gcd = i;
			i = i % j;
			j = gcd;
			if (gcd == 1)
				return true;
		}

		// TODO Auto-generated method stub
		return false;
	}

}
