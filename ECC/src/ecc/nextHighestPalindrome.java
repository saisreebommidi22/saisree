package ecc;

import java.util.Scanner;

public class nextHighestPalindrome {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();
		
		num++;

		System.out.println(getNextPalindrome(num));
	}

	public static int reverseOfNumber(int num) {

		int reverse = 0;

		while (num > 0) {

			int remainder = num % 10;

			reverse = reverse * 10 + remainder;

			num /= 10;

		}

		return reverse;
	}

	public static boolean isPalindrome(int num) {

		return (num == reverseOfNumber(num));
	}

	public static int getNextPalindrome(int num) {
		
		while(!isPalindrome(num)){
			
			num++;
		}
		return num;
	}



}
