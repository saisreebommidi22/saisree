package ecc;

import java.util.Scanner;

public class twoOutOfThreeBooleanArgumentsAreTrue {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		boolean a = sc.nextBoolean();
		boolean b = sc.nextBoolean();
		boolean c = sc.nextBoolean();

		System.out.println(getBoolean(a, b, c));

	}

	public static boolean getBoolean(boolean a, boolean b, boolean c) {

		if (a && b && c)

			return false;
		
		else if( (a && b)|| (b && c)|| (a && c))
			
			return true;
		else 	

			return false;

	}

}
