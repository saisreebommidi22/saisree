package ecc;

public class TwinPrimes01 {

	public static void main(String[] args) {

		int num1 = 1;
		int num2 = 200;

		System.out.println(twinPrimes(num1, num2));
	}

	public static String twinPrimes(int start, int limit) {

		// ADD YOUR CODE HERE
		String res = "";
		if (start <= 0 || limit <= 0)

			return "-1";

		else if (start >= limit)

			return "-2";
		else {

			for (int i = start; i < limit; i++) {

				if (isPrime(i) && isPrime(i + 2)) {

					res = res + (i) + ":" + (i + 2) + ",";
				}

			}
			if (res.isEmpty())
				
				return "-3";

				return res.substring(0, res.length() - 1);

		}

	}

	public static boolean isPrime(int num) {
		// ADD YOUR CODE HERE
		int count = 0;

		for (int i = 1; i <= num; i++) {

			if (num % i == 0) {

				count++;
			}
		}
		if (count == 2)

			return true;
		else
			return false;
	}

}
