package ecc;

import java.util.Scanner;

public class sumOfUniqueNumbers {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size: ");

		int size = sc.nextInt();

		int[] arr = new int[size];

		for (int i = 0; i < size; i++) {

			arr[i] = sc.nextInt();
		}
		System.out.println("Sum "+getUniqueElements(arr));

	}

	public static int getUniqueElements(int[] arr){
		
			int sum = 0;
			
			for (int i = 0; i < arr.length; i++) {
				
				int count = 0;

				for (int j = 0; j < arr.length; j++) {
					
					if (arr[i] == arr[j])

						count++;
				}
				if (count == 1) {
					
					System.out.println(arr[i]);
					
					sum = sum +arr[i];

				}
			}
			return sum;
	}

}
