package ecc;

public class JulianDate01 {

	public static void main(String[] args) {
		
		String date = "23-DEC-2016";
		
		System.out.println(dateFormat(date));
	}

	public static String dateFormat(String date) {

		String[] dateParts = date.split("-");
		
		int dd = Integer.parseInt(dateParts[0]);
		int mon = convertMMMtoMM(dateParts[1]);
		int yyyy = Integer.parseInt(dateParts[2]);
		
		return yyyy + julianDate(dd, mon);
		
		// ADD YOUR CODE HERE
	}

	public static String julianDate(int dd, int mon) {

		int[] MONTHS = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
		
		int jd = MONTHS[mon - 1] + dd;
		
		String day = "" + jd;
		
		if (jd <= 9)
			day = "00" + jd;
		else if (jd <= 99) {
			day = "0" + jd;
		}
		return day;
		// ADD YOUR CODE HERE
	}

	public static int convertMMMtoMM(String mon) {

		String months = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
		
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();
		
		return ((months.indexOf(mon) / 3) + 1);
		// ADD YOUR CODE HERE
	}

}
