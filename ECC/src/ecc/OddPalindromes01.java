package ecc;

public class OddPalindromes01 {
	
	 public static void main(String[] args) {
		 
	        int num1 = 1500;
	        int num2 = 2000;
	        
	        System.out.println(generateOddPalindromes(num1, num2));
	    }

	    public static String generateOddPalindromes(int start, int limit) {
	    	
	    	String result = "";
	    	
	    	if( start <= 0 || limit <= 0)
	    		
	    		return "-1";
	    	
	    	else if(start >= limit)
	    		
	    		return "-2";
	    	else{
	    		
	    		for(int s = start; s < limit; s++){
	    			
	    			if(isPalindrome(s) && isAllDigitsOdd(s))
	    				
	    				result = result +s +",";
	    		}
	    		
	    		if(result.isEmpty())
	    			
	    			return "-3";
	    	}
	    	
	    	return result.substring(0,result.length()-1);
	        //ADD YOUR CODE HERE
	    }

	    public static boolean isPalindrome(int num) {
	    	
	    	
	    	if(num == reverse(num))
	    		
	    		return true;
	    	
	    	return false;
	        
	    }

	    public static int reverse(int num) {
	    	
	    	int reverse = 0;
	    	
	    	while(num > 0){
	    		
	    		int remainder = num % 10;
	    		
	    		reverse = reverse * 10 + remainder;
	    		
	    		num = num/10 ; 
	    		
	    		
	    	}
	    	return reverse;
	    }

	    public static boolean isAllDigitsOdd(int num) {
	    	
	    	while(num > 0){
	    		
	    		int digit = num % 10;
	    		
	    		if(digit % 2 == 0 && digit!=0)
	    			return false;
	    		num = num/10;
	    	}
	    	return true;
	    }


}
