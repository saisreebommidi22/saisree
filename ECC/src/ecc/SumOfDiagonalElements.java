package ecc;

import java.util.Scanner;

public class SumOfDiagonalElements {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int arr[][] = new int[3][3];

		System.out.println("Enter array elements : ");

		for (int i = 0; i < 3; i++) {

			for (int j = 0; j < 3; j++) {

				arr[i][j] = sc.nextInt();

			}
		}

		getSumOfDiagonals(arr);
	}

	public static void getSumOfDiagonals(int[][] arr) {

		int sum = 0;

		for (int i = 0; i < arr.length; i++) {

			for (int j = 0; j < arr.length; j++) {

				if (i == j) {

					System.out.println(arr[i][j]);
					sum = sum + arr[i][j];
				}
			}
		}
		System.out.println("Sum of the diagonals is " + sum);
	}
}
