package ecc;

import java.util.Scanner;

public class NextPalindrome {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number ");

		int num = sc.nextInt();

		getNextPalindrome(num);
	}

	public static void getNextPalindrome(int num) {

		for (int i = num + 1; i > 0;i++) {
			
			int reverse = 0;
			int temp = i;

			while (temp > 0) {

				int digit = temp % 10;
				reverse = reverse * 10 + digit;
				temp= temp / 10;
			}
			if (reverse == i) {
				System.out.println("NextPalindrome is :" + reverse);
				break;
			} 
			else
				continue;
		}

	}

}
