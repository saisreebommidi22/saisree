package ecc;

import java.util.Scanner;

public class AliquotNumber {
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		System.out.println(getSumOfFactors(num));
		
	}
	public static int getSumOfFactors(int num){
		
		int sum = 0;
		
		for( int i =1; i < num ; i++){
			
			if(num%i == 0){
				
				sum = sum + i;
			}
		}
		return sum;
	}

}
