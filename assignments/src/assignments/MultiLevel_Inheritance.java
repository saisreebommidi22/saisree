package assignments;

class X
{
	int a= 20;
	public void methodX()
	{
		System.out.println("Class X method");
	}
}
class Y extends X
{
	int b = 21;
	public void methodY()
	{
		System.out.println("class Y method");
	}
}
class MultiLevel_Inheritance extends Y
{
	public void methodZ()
	{
		System.out.println("class Z method");
	}
	public static void main(String args[])
	{
		MultiLevel_Inheritance obj = new MultiLevel_Inheritance();
		System.out.println(obj.a);
		obj.methodX();
		System.out.println(obj.b);//calling grand parent class method
		obj.methodY(); //calling parent class method
		obj.methodZ(); //calling local method
	}
}
