package assignments;  
  
class vehicle 
{ 
	int a =20;
    public void wheels() 
    { 
        System.out.println("Every vehicle will have wheels"); 
    } 
} 
  
class car extends vehicle 
{ 
	int b=23;
    public void no_of_wheels() 
    { 
        System.out.println("A car will have four wheels"); 
    } 
} 
public class Single_Inheritance 
{ 
    public static void main(String[] args) 
    { 
        car c = new car();
        System.out.println(c.a);
        c.no_of_wheels(); 
        System.out.println(c.b);
        c.wheels();
    } 
} 