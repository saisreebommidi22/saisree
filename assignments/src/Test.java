
class Test {

		public static void main(String[] args) {
			int n = 10;
			print(n);
		}

		public static void print(int n) {
			int count = 0;
			int i = 0;
			while (count <= n) {
				if (i % 2 == 0) {
					count++;
					System.out.print(i+" ");
				}
				i++;
			}

		}
}