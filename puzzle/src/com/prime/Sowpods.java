package com.prime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sowpods {

	public static void main(String[] args) throws IOException{

		Map<String, List<String>> hmap = new HashMap<>();

		List<String> anagrams;

		File file = new File("/home/user/Downloads/sowpods.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));

		String word;

		while ((word = br.readLine()) != null) {

			String sorted_word = sort(word);

			if (hmap.containsKey(sorted_word)) {

				hmap.put(sorted_word, hmap.get(sorted_word)).add(word);
			
			} else {
				
				anagrams = new ArrayList();

				anagrams.add(word);

				hmap.put(sorted_word, anagrams);

			}

		}
		
		for(Map.Entry<String, List<String>> entry : hmap.entrySet() ){
			
			if(entry.getValue().size() > 1)
			
				System.out.println(entry.getValue());
		}
	}

	private static String sort(String word) {

		char[] sortedArray = word.toCharArray();

		Arrays.sort(sortedArray);

		String sortedWord = new String(sortedArray);

		return sortedWord;
	}

}
