import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class Othello2 {

	static Scanner sc = new Scanner(System.in);

	static char Player = 'x';

	public static void main(String[] args) throws IOException {

		char[][] othello = setOthello1();

		displayOthello(othello);

		System.out.println("\nEnter player to start :-");
		initCurMove();

		boolean loop = true;

		while(loop) {

			System.out.println("ENTER 'L' TO  LIST LEGAL MOVES \nENTER 'Q' TO QUIT");
			String command =  sc.nextLine().toUpperCase();

			if(command.charAt(0) == 'L') {
				List<String> moves = getValidMoves1(othello);

				displayMoves(moves);

				if(!moves.isEmpty()) {
					System.out.print("Enter move \n >>> ");
					String inputMove = sc.nextLine();

					othello = doPlayerMove(inputMove, moves, othello);
					displayOthello(othello);

				}
				displayCountBW(othello);
				changePlayer();
			}
			else  {
				System.out.println("Goodbye!");
				loop = false;
			}	
		}

		sc.close();
	}


	public static char[][] setOthello1() throws IOException {

		FileReader fr = new FileReader("/home/dell/Desktop/othelloInput1");
		BufferedReader br = new BufferedReader(fr);
		char[][] othello = new char[8][8];
		String str;

		int i = 0;
		while((str = br.readLine()) != null){
			othello[i] = str.toCharArray();
			i++;
		}

		br.close();
		fr.close();	

		return othello;
	}


	public static void initCurMove() {
		System.out.print(">>> ");
		String str = sc.nextLine().toUpperCase();

		setPlayer(str.charAt(0));

	}


	public static void displayOthello(char[][] othello) {

		System.out.print("  ");
		for(int i = 1; i < 9; i++)
			System.out.print(i + " ");
		System.out.println();

		int var = 1;
		for(int i = 0; i < 8; i++) {
			System.out.print(var++ + " ");
			for(int j = 0; j < 8; j++) {
				System.out.print(othello[i][j] + " ");
			}
			System.out.println();

		}
	}


	public static List<String> getValidMoves1(char[][] othello) {

		char player = getPlayer();
		char opponent = (player == 'W')? 'B':'W';

		List<String> moves = new ArrayList<>();

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				char var = othello[i][j];
				if (var == opponent) {
					List<String> list = new ArrayList<>();

					if(i > 0)
						if (othello[i - 1][j] == '-' ) {

							if (!(list = isFlippable(i - 1 , j, othello)).isEmpty()) {

								moves.addAll(list);
							}
						}

					if(i < 7)
						if (othello[i + 1][j] == '-')
							if (!(list = isFlippable(i + 1 , j, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(j > 0)
						if (othello[i][j - 1] == '-')
							if (!(list = isFlippable(i , j - 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(j < 7)
						if (othello[i][j + 1] == '-')
							if (!(list = isFlippable(i , j + 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(i > 0 && j > 0)
						if(othello[i - 1][j - 1] == '-')
							if (!(list = isFlippable(i - 1 , j - 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(i > 0 && j > 8)
						if(othello[i - 1][j + 1] == '-')
							if (!(list = isFlippable(i - 1 , j + 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(i < 7 && j > 0)
						if(othello[i + 1][j - 1] == '-')
							if (!(list = isFlippable(i + 1 , j - 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

					if(i < 7 && j < 7)
						if(othello[i + 1][j + 1] == '-')
							if (!(list = isFlippable(i + 1 , j + 1, othello)).isEmpty()) {

								moves.addAll(list);
							}

				}
			}
		}

		return moves;
	}


	public static List<String> isFlippable(int x, int y, char[][] othello) {

		char player = getPlayer();

		//char opponent = (player == 'W')? 'B':'W';

		List<String> moves = new ArrayList<>();
		char[][] temp2d = new char[8][8];

		for(int i =0; i < 8; i++) {
			for(int j = 0; j < 8; j++)
				temp2d[i][j] = othello[i][j];
		}

		temp2d[x][y] = player;
		String regex = (player == 'W') ? "WB+W" : "BW+B";

		int i = x, j = y;
		String str = "" ;
		while ( i >= 0 && !str.contains("-")) {
			str += temp2d[i--][j];

			if (Pattern.matches(regex, str))
				moves.add("a" + x + "" + y + "" + (str.length() - 2));
		}


		str = "";
		i = x; j = y;
		while (i < 8 && !str.contains("-")) {
			str += temp2d[i++][j];

			if (Pattern.matches(regex, str))
				moves.add("b" + x + "" + y + (str.length() - 2));
		}



		str = "";
		i = x; j = y;
		while ( j >= 0 && !str.contains("-")) {
			str += temp2d[i][j--];

			if (Pattern.matches(regex, str))
				moves.add("l" + x + "" + y + (str.length() - 2));
		}


		str = "";
		i = x; j = y;
		while ( j < 8 && !str.contains("-")) {
			str += temp2d[i][j++];
			if (Pattern.matches(regex, str))
				moves.add("r" + x + "" + y + (str.length() - 2));
		}


		str = "";
		i = x; j = y;
		while ((i >= 0 && j >= 0) && !str.contains("-")) {
			str += temp2d[i--][j--];
			if (Pattern.matches(regex, str))
				moves.add("w" + x + "" + y + (str.length() - 2));
		}

		str = "";
		i = x; j = y;
		while ((i >= 0 && j < 8) && !str.contains("-")) {
			str += temp2d[i--][j++];
			if (Pattern.matches(regex, str))
				moves.add("x" + x + "" + y + (str.length() - 2));
		}


		str = "";
		i = x; j = y;
		while ( (i < 8 && j >= 0) && !str.contains("-")) {
			str += temp2d[i++][j--];
			if (Pattern.matches(regex, str))
				moves.add("y" + x + "" + y + (str.length() - 2));
		}

		str = "";
		i = x; j = y;
		while ( (i < 8 && j < 8) && !str.contains("-")) {
			str += temp2d[i++][j++];
			if (Pattern.matches(regex, str))
				moves.add("z" + x + "" + y + (str.length() - 2));
		}

		return moves;
	}


	public static void displayMoves(List <String> moves) {

		if(moves.isEmpty()) {
			System.out.println("No Legal Moves");
			//changePlayer();
		}
		else {
			HashSet<String> distinct = new HashSet<>(); 


			for (String x: moves) {
				if (!distinct.contains(x.substring(1,3)))

					distinct.add(x.substring(1,3));
			}

			for(String x: distinct) {
				System.out.println("(" + (Integer.valueOf(x.substring(0,1)) + 1) + "," 
						+ (Integer.valueOf(x.substring(1, 2)) + 1) + ")");
			}
			//for(String x: moves) {
			//System.out.println(x);
			//}
		}

	}


	public static char[][] doPlayerMove(String input, List<String> moves, char[][] othello) {

		int i = Integer.valueOf(input.substring(1,2)) - 1;
		int j = Integer.valueOf(input.substring(2)) - 1;
		int flips = 0;
		char dir = 'x';

		List<String> plMoves = new ArrayList<>();

		String index = i + "" + j;
		for(String x: moves) {

			String var = x.substring(1, 3);
			dir = x.charAt(0);
			flips = Integer.valueOf(x.substring(3));

			if(index.equals(var) ){
				othello = doFlip(othello, i, j, dir, flips);
				plMoves.add(x);
			}
		}
		//changePlayer();
		return othello;
	}


	public static char[][] doFlip (char[][] puzzle, int i, int j, char dir, int flips) {

		char player = getPlayer();

		char[][] othello = new char[8][8];

		for(int m =0; m < 8; m++) {
			for(int n = 0; n < 8; n++)
				othello[m][n] = puzzle[m][n];
		}


		othello[i][j] = player;
		int x = 1;

		if(dir == 'a') {
			while(x <= flips ) {
				othello[i - x][j] = player;
				x++;
			}	
		}
		else if(dir == 'b') {
			x = 1;
			while(x <= flips ) {
				othello[i + x][j] = player;
				x++;
			}	
		}
		else if(dir == 'l') {
			x = 1;
			while(x <= flips ) {
				othello[i][j - x] = player;
				x++;
			}	
		}
		else if(dir == 'r') {
			x = 1;
			while(x <= flips ) {
				othello[i][j + x] = player;
				x++;
			}	
		}
		else if(dir == 'w'){
			x = 1;
			while(x <= flips ) {
				othello[i - x][j - x] = player;
				x++;
			}
		}
		else if(dir == 'x'){
			x = 1;
			while(x <= flips ) {
				othello[i - x][j + x] = player;
				x++;
			}
		}
		else if(dir == 'y'){
			x = 1;
			while(x <= flips ) {
				othello[i + x][j - x] = player;
				x++;
			}
		}
		else if(dir == 'z'){
			x = 1;
			while(x <= flips ) {
				othello[i + x][j + x] = player;
				x++;
			}
		}

		return othello;

	}


	public static void displayCountBW(char[][] othello) {

		int countB = 0, countW = 0;

		for(int i =0; i < 8; i++) {
			for(int j =0; j < 8; j++) {		
				if(othello[i][j] == 'W')
					countW++;
				if(othello[i][j] == 'B')
					countB++;
			}
		}

		System.out.println("Black - " + countB + " White - " + countW);
	}

	public static void setPlayer(char var) {
		Player = var;
	}

	public  static char getPlayer() {
		return Player;
	}

	public static void changePlayer() {

		if(Player == 'W')
			setPlayer('B');
		else if (Player == 'B')
			setPlayer('W');

		System.out.println("Player Changed to: " + getPlayer());
	}

}
