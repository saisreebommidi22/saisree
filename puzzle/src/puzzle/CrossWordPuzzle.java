package puzzle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class CrossWordPuzzle {

	public static void main(String[] args) {

		char[][] crossword = initializeCrossword();
		
		int[][] numberedCrossword = numberBeingCrossword(crossword);
		
		display(crossword, numberedCrossword);
				
		System.out.println(getCrossWordsUp(crossword,numberedCrossword));
		
		System.out.println(getCrossWordsDown(crossword,numberedCrossword));
		
		
	}

	public static char[][] initializeCrossword() {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		char puzzle[][] = new char[8][9];

		System.out.println(" Enter array elements ");

		for (int i = 0; i < puzzle.length; i++) {

			for (int j = 0; j < puzzle[i].length; j++) {

				puzzle[i][j] = sc.next().charAt(0);
			}
		}
		return puzzle;

	}

	public static int[][] numberBeingCrossword(char[][] crossword) {

		int[][] numberedCrossword = new int[8][9];
		int count = 1;

		for (int i = 0; i < numberedCrossword.length; i++) {

			for (int j = 0; j < numberedCrossword[i].length; j++) {

				if (crossword[i][j] != '*') {

					if (i == 0 || j == 0) {

						numberedCrossword[i][j] = count;
						count++;
						continue;
					}

					if ((crossword[i - 1][j] == '*') || (crossword[i][j - 1] == '*')) {

						numberedCrossword[i][j] = count;
						count++;
					}
				}
			}
		}
		return numberedCrossword;
	}

	public static void display(char[][] crossword, int[][] numberedCrossword) {

		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < 9; j++) {

				System.out.print(crossword[i][j] + " ");
			}
			System.out.println(" ");
		}
		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < 9; j++) {

				System.out.print(numberedCrossword[i][j] + " ");
			}
			System.out.println("");
		}

	}

	public static List<String> getCrossWordsUp(char[][] crossword, int[][] numberedCrossWord) {

		String s;
		
		List<String> wordList = new ArrayList<String>();

		int num;

		for (int i = 0; i < crossword.length; i++) {
			
			s = "";
			
			for (int j = 0; j < crossword[i].length; j++) {

				if (crossword[i][j] != '*') {

					s = s + crossword[i][j];

				} else {

					if (s != "") {

						num = numberedCrossWord[i][j - s.length()];
						wordList.add(num+"."+s);
						s = "";
					}
				}

			}
		}
		return wordList;

	}
	public static Map<Integer,String> getCrossWordsDown(char[][] crossword, int[][] numberedCrossWord) {

		String s;

		Map<Integer,String> wordList = new TreeMap<Integer,String>();
		int num;

		for (int j = 0; j < crossword.length; j++) {
			
			s = "";
			
			for (int i = 0; i < crossword.length; i++) {

				if (crossword[i][j] != '*') {

					s = s + crossword[i][j];

				} else {

					if (s != "") {

						num = numberedCrossWord[i - s.length()][j];
						wordList.put(num,s);

						s = "";
					}
				}

			}
		}
		return wordList;

	}

}
