package puzzle;

import java.util.Scanner;

public class Puzzle1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		char[][] puzzle = initializingPuzzle();

		displayPuzzle(puzzle);

		System.out.println("Enter a move A-Above ,B-Below, L-Left, R-Right");

		char move = sc.next().charAt(0);
		char[][] updatedPuzzle = puzzle;

		while (!isWinning(updatedPuzzle) && isValidMove(updatedPuzzle, move)) {

			updatedPuzzle = swapElements(puzzle, move);

			displayPuzzle(updatedPuzzle);

			System.out.println("Enter a move A-Above ,B-Below, L-Left, R-Right");

			move = sc.next().charAt(0);

		}
		if (isWinning(updatedPuzzle)) {
			System.out.println("you won the game");
		}

		else if (!isValidMove(puzzle, move))

			System.out.println(" Bad move Game over");
	}

	public static char[][] initializingPuzzle() {

		Scanner sc = new Scanner(System.in);

		char puzzle[][] = new char[5][5];

		System.out.println(" Enter array elements ");

		for (int i = 0; i < puzzle.length; i++) {

			for (int j = 0; j < puzzle[i].length; j++) {

				puzzle[i][j] = sc.next().charAt(0);
			}
		}
		return puzzle;

	}

	public static void displayPuzzle(char[][] puzzle) {

		for (int i = 0; i < puzzle.length; i++) {

			for (int j = 0; j < puzzle[i].length; j++) {

				System.out.print(puzzle[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static boolean isWinning(char[][] puzzle) {

		String str = "ABCDEFGHIJKLMNOPQRSTUVWX";
		String str1 = "";

		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle[i].length; j++) {

				str1 = str1 + puzzle[i][j];
			}
		}
		str1 = str1.replaceAll("_", " ");

		if (str.equalsIgnoreCase(str1.trim())) {
			return true;
		}
		return false;

	}

	public static int[] findSpacePosition(char[][] puzzle) {

		int[] position = new int[2];

		for (int i = 0; i < puzzle.length; i++) {

			for (int j = 0; j < puzzle.length; j++) {

				if (puzzle[i][j] == '_') {

					position[0] = i;
					position[1] = j;
					return position;
				}
			}

		}
		return null;
	}

	public static boolean isValidMove(char[][] puzzle, char move) {

		int[] position = findSpacePosition(puzzle);
		int row = position[0];
		int col = position[1];

		if (move == 'A') {

			return (row - 1 >= 0);

		}
		if (move == 'B') {
			return row + 1 < puzzle.length;
		}

		if (move == 'L') {
			return col - 1 >= 0;
		}
		if (move == 'R') {
			return col + 1 < puzzle.length;
		}
		return false;
	}

	public static char[][] swapElements(char[][] puzzle, char move) {

		int[] position = findSpacePosition(puzzle);
		int row = position[0];
		int col = position[1];
		char temp;

		if (move == 'A') {

			temp = puzzle[row][col];
			puzzle[row][col] = puzzle[row - 1][col];
			puzzle[row - 1][col] = temp;
		}
		if (move == 'B') {

			temp = puzzle[row][col];
			puzzle[row][col] = puzzle[row + 1][col];
			puzzle[row + 1][col] = temp;
		}
		if (move == 'L') {

			temp = puzzle[row][col];
			puzzle[row][col] = puzzle[row][col - 1];
			puzzle[row][col - 1] = temp;
		}
		if (move == 'R') {

			temp = puzzle[row][col];
			puzzle[row][col] = puzzle[row][col + 1];
			puzzle[row][col + 1] = temp;
		}

		return puzzle;

	}
}
