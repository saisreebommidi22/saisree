package puzzle;

import java.util.*;

public class Othello {

	private static   char black_player = 'b';
	private static  char white_player = 'w';
	private static  char[][] game = new char[8][8];

	public Othello(char black_player, char white_player, char[][] game) {

		super();
		this.black_player = black_player;
		this.white_player = white_player;

		this.game = game;

		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < game[i].length; j++) {

				game[i][j] = '_';
			}
		}
		game[3][3] = game[4][4] = 'w';
		game[4][3] = game[3][4] = 'b';

	}

	public static void displayOthello(char game[][]) {

		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < game[i].length; j++) {

				System.out.print(game[i][j] + " ");
			}
			System.out.println(" ");
		}

	}

	public static void gameOver() {

		int countb = 0, countw = 0;

		for (int i = 0; i < 8; i++) {

			for (int j = 0; j < game[i].length; j++) {

				if (game[i][j] == black_player) {

					countb++;
				} else if (game[i][j] == white_player) {

					countw++;
				}

			}
		}
		if (countb > countw) {

			System.out.println("Winner is black player");
		} else if (countb < countw) {

			System.out.println("Winner is white player");
		} else
			System.out.println("Tie Game");

	}
	public static boolean isGameOver(){
		
		for(int i = 0 ; i < 8; i++){
			
			for(int j = 0; j< game[i].length; j++){
				
				if(game[i][j]!='_')
					
					return true;
					
				
			}
		}
		return false;
	}

}